import { Component, OnInit } from '@angular/core';
import { AuthService} from 'src/app/services/auth/auth.service';
import { SessionService} from 'src/app/services/session/session.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  user = {
    username: '',
    password: ''
  }

  isLoading: boolean = false;
  
  registerError: string;

  constructor(private auth: AuthService, private session: SessionService, private router: Router) { 
    if (this.session.get() !==false){
      this.router.navigateByUrl('/dashboard');

    }
  }

  ngOnInit(): void {
  }
  async onRegisterClicked(){
    this.registerError ='';
    try {

      this.isLoading=true;

      const result: any = await this.auth.register(this.user)
      
      if(result.status < 400){
        this.session.save({
          token: result.data.token, 
          username: result.data.user.username
        });
        this.router.navigateByUrl('/dashboard')
      }

    }
    catch (e) {
      console.log(e.error);
      this.registerError = e.error.error;
    }finally{
    this.isLoading=false;
    }
  
  }

}
